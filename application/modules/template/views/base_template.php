<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Justified Nav Template for Bootstrap</title>

        <?php $this->carabiner->display('default', 'css'); ?>

    </head>

    <body>

        <div class="container">

            <?php $this->load->view('template_inc/top_nav'); ?>

            <div class="body-content" style="min-height: 465px;">

                <?php $this->load->view($module . '/' . $view_file);?>

            </div><!-- /.body-content -->

            <!-- Site footer -->
            <?php $this->load->view('template_inc/footer'); ?>

        </div> <!-- /container -->

        <!-- Load all JS -->
        <?php $this->carabiner->display('default', 'js'); ?>

    </body>
</html>