<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Default</title>
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Le javascript 
        <script src="http://code.jquery.com/jquery-1.5.2.min.js"></script>
        <link rel="stylesheet" href="{$smarty.const.BASE}public/fbootstrapp/css/bootstrap.css">-->
        <style>
            h1, h2, h3, h4, h5{
                color: #222;
            }
            body{
                background:#f5f5f5;
                font: 0.75em/1.5em Arial, Helvetica, sans-serif;
                color: #333333;
            }
            header{
                padding: 20px;
                background:#bfc9ca;
                border-bottom:1px solid #ddd;
            }
            footer{
                padding:20px;
                background:#ddd;
                border-top:1px solid #ddd;
            }
            .container{
                padding:20px;
                height: 400px;
            }
            table.table{
                width:100%;
            }
            table, td, th{
                border-collapse:collapse;
                padding:5px;
                border:1px solid #ddd;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>HMVC Stuff</h1>
        </header>
        <div class="container">
            <div class="row">
                <div class="span12">
                    
                    <?php
                        echo Modules::run('top_nav/top_nav/index');
                        $this->load->view($module . '/' . $view_file);
                    ?>

                </div>
            </div>
        </div> <!-- /container -->
        <footer>
            <p>&copy; Company 2013 sanojbogoda@yahoo.com</p>
        </footer>
    </body>
</html>
